<?php
// mysql_connect('localhost', 'root', 'sylwia123');
// mysql_select_db('upc');

function calculateLat($y, $z) {
    $pi = '3.14159265359';
    $n = (string) pow(2, $z);

    $g1 = bcmul('2', (string) $y, 15);
    $l1 = bcdiv($g1, $n, 15);
    $a1 = bcsub('1', $l1, 15);
    $var = bcmul($pi, $a1, 15);

    return rad2deg(atan(sinh($var)));
}

function calculateLon($x, $z) {
    $n = (string) pow(2, $z);
    $a1 = bcdiv((string) $x, (string) $n, 15);
    $a2 = bcmul($a1, '360.0', 15);
    $a3 = bcsub($a2, '180.0', 15);

    return $a3;
}

function calculateY($lat, $z) {
    $pi = '3.14159265359';

    $a1 = (string) cos(deg2rad($lat));
    $a2 = (string)  tan(deg2rad($lat));
    $a3 = bcdiv('1', $a1, 15);
    $a4 = (string) log($a2 + $a3);
    $a5 = bcdiv($a4, $pi, 15);
    $a6 = bcsub('1', $a5, 15);
    $a7 = bcdiv($a6, '2', 15);
    $a8 = (string) pow(2, $z);
    $a9 = bcmul($a7, $a8, 15);

    return $a9;
}

function calculateX($lon, $z) {
    $a1 = bcadd((string) $lon, '180', 15);
    $a2 = bcdiv($a1, '360', 15);
    $a3 = (string) pow(2, $z);
    $a4 = bcmul($a2, $a3, 15);

    return $a4;
}


function draw($draw, $size, $x, $y) {
    // $x = 195
    // $y = 756.5
    // $size = 19.5
    $size /= 100000;
    // Array ( [0] => Array ( [x] => 4.86219 [y] => 52.37087 ) [1] => Array ( [x] => 4.85819 [y] => 52.37779820323 ) [2] => Array ( [x] => 4.85019 [y] => 52.37779820323 ) [3] => Array ( [x] => 4.84619 [y] => 52.37087 ) [4] => Array ( [x] => 4.85019 [y] => 52.36394179677 ) [5] => Array ( [x] => 4.85819 [y] => 52.36394179677 ) )
    // print_r($size);exit;
    // for($i=0; $i<6; $i++) {
    //     $coordinates[] = array(
    //         // 195 + 9.5*cos(i*2*pi()/6)
    //         'x' => $x + $size*cos($i*2*pi()/6),
    //         'y' => $y + $size*sin($i*2*pi()/6)
    //     );
    // }
    // Array ( [0] => Array ( [0] => Array ( [x] => 4.85419 [y] => 52.37887 ) [1] => Array ( [x] => 4.8680464064606 [y] => 52.37487 ) [2] => Array ( [x] => 4.8680464064606 [y] => 52.36687 ) [3] => Array ( [x] => 4.85419 [y] => 52.36287 ) [4] => Array ( [x] => 4.8403335935394 [y] => 52.36687 ) [5] => Array ( [x] => 4.8403335935394 [y] => 52.37487 ) [6] => Array ( [x] => 4.85419 [y] => 52.37887 ) ) )
    // $coordinates = array(
                
    //               ['x' =>$x, 'y' =>$y+$size],
    //               ['x' =>$x+(sqrt(3)*$size), 'y' =>$y+0.5*$size],
    //               ['x' =>$x+(sqrt(3)*$size), 'y' =>$y-0.5*$size],
    //               ['x' =>$x, 'y' =>$y-$size],
    //               ['x' =>$x-(sqrt(3)*$size), 'y' =>$y-0.5*$size],
    //               ['x' =>$x-(sqrt(3)*$size), 'y' =>$y+0.5*$size],
    //               ['x' =>$x, 'y' =>$y+$size]

                
    //         );
    $feature = array(
            'type' => 'Feature',
            'geometry' => array(
                'type' => 'Polygon',
                'coordinates' => array(
                    [
                      [$x, $y+$size],
                      [$x+(sqrt(3)*$size), $y+0.5*$size],
                      [$x+(sqrt(3)*$size), $y-0.5*$size],
                      [$x, $y-$size],
                      [$x-(sqrt(3)*$size), $y-0.5*$size],
                      [$x-(sqrt(3)*$size), $y+0.5*$size],
                      [$x, $y+$size],
                    ]
                )
            ),
        );
    return $feature;
    // print_r($coordinates);exit;
    // $draw->setFillOpacity(0.7);
    // $draw->polygon($coordinates);
}


// Get vars
$tile_x = @$_GET['x'];
$tile_y = @$_GET['y'];
$tile_z = @$_GET['z'];
$zoom = $_GET['z'];
$size = 15;
$a = $size / 2;
$b = $a / 2;
$c = sqrt( ($a * $a) + ($b * $b) );
$width = ($b + $b + $c);
$height = ($b + $b + $c);
    
// Create image
$image = new Imagick();
$image->newImage(768, 768, new ImagickPixel( 'transparent' ));
$draw = new ImagickDraw();

// Calc tile range
$rangexy['topleft']['x'] = $tile_x - 1;
$rangexy['topleft']['y'] = $tile_y - 1;

$rangexy['topright']['x'] = $tile_x + 2;
$rangexy['topright']['y'] = $tile_y - 1;

$rangexy['bottomleft']['x'] = $tile_x - 1;
$rangexy['bottomleft']['y'] = $tile_y + 2;

$rangexy['bottomright']['x'] = $tile_x + 2;
$rangexy['bottomright']['y'] = $tile_y + 2;

foreach ($rangexy as $key => $loc) {
    $rangelatlng[$key]['lat'] = calculateLat($loc['y'], $tile_z);
    $rangelatlng[$key]['lng'] = calculateLon($loc['x'], $tile_z);
}

// Get all wifispots in range
// $query = mysql_query('
//     SELECT lat, lng
//     FROM spots
//     WHERE
//     lat BETWEEN ' . $rangelatlng['bottomleft']['lat'] . ' AND ' . $rangelatlng['topleft']['lat'] . '
//     AND
//     lng BETWEEN ' . $rangelatlng['topleft']['lng'] . ' AND ' . $rangelatlng['topright']['lng'] . '
// ');
$m = new MongoClient();
$db = $m->upc2;
$spotsCollection = new MongoCollection($db, 'spots');

$queryMongo = array("lat" => array('$gt' => (string)$rangelatlng['bottomleft']['lat'],
                                   '$lt' => (string)$rangelatlng['topleft']['lat']),
                    "lng" => array('$gt' => (string)$rangelatlng['topleft']['lng'],
                                   '$lt' => (string)$rangelatlng['topright']['lng'])
        );
// print_r($rangelatlng['bottomleft']['lat']);
// print_r($rangelatlng['topleft']['lat']);
// print_r($rangelatlng['topleft']['lng']);
// print_r($rangelatlng['topright']['lng']);


$cursor = $spotsCollection->find()->limit(1000);

$spotsdraw = array();
// while ($spostss = mysql_fetch_assoc($query)) {
foreach($cursor as $spot => $val){
    $precision = 15;

    // Calc x and y for spot
    $spot_x = (string) calculateX($val['lng'], $tile_z);
    $spot_y = (string) calculateY($val['lat'], $tile_z);
    
    // Remove tile width
    $spot_x_in_tile = (string) bcsub($spot_x, $rangexy['bottomleft']['x'], $precision);
    $spot_y_in_tile = (string) bcsub($spot_y, $rangexy['topleft']['y'], $precision);
    
    // Calculate pixels
    $spot_left_in_tile = ((string) bcmul($spot_y_in_tile, '256', 5));
    $spot_top_in_tile = ((string) bcmul($spot_x_in_tile, '256', 5));
    
    // Group to hexagons
    $w = (string) ($size);
    $h = (string) ($size) + 2;

    $hex_left_in_tile = bcmul(bcdiv($spot_left_in_tile, $h, 0), $h, 0);
    $hex_top_in_tile = bcmul(bcdiv($spot_top_in_tile, $w, 0), $w, 0);


    // Calculate pixels in total map
    $spot_top_in_map = round((string) bcmul($spot_x, '1', 5));
    // echo $spot_top_in_map;  exit;
    // $hex_top_in_map = bcmul((bcdiv($spot_top_in_map, $w, 0)), $w, 0);

    $spot_top_in_map = $hex_top_in_tile;

    // Add to draw array
    if ( ! isset($spotsdraw[$hex_top_in_tile]))
        $spotsdraw[$hex_top_in_tile] = array(
            'map_hex' => array(
                'top' => $spot_top_in_map,
                'coords' => array("lng"=>$val['lng'], "lat"=>$val['lat'])
            )
        );

    if ( ! isset($spotsdraw[$hex_top_in_tile][$hex_left_in_tile]))
        $spotsdraw[$hex_top_in_tile][$hex_left_in_tile] = array('count' => 0);
    
    $spotsdraw[$hex_top_in_tile][$hex_left_in_tile]['count']++;
}

// Draw
ksort($spotsdraw);
// print_r($spotsdraw); exit;
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);
foreach ($spotsdraw as $spottop => $spots) {
    if ($rangexy['bottomleft']['x'] % 2 == 0)
        $fix = ($spots['map_hex']['top'] % 2 == 0);
    else
        $fix = ! ($spots['map_hex']['top'] % 2 == 0);

    # $fix = ($spots['map_hex']['top'] % 2 == 0);

    foreach ($spots as $spotleft => $data) {
        if ($spotleft == 'map_hex') continue;

        if ($fix) {
            $spotleft = ($spotleft + ($size / 2) + 1);
        }
        
        // make larger
        // $dsize = bcmul($zoom, 1.30, 3);
        // print_r($dsize);exit;
         // $dsize = $size;
        $c = draw($draw, $zoom, $spots['map_hex']['coords']['lng'], $spots['map_hex']['coords']['lat']);

        
        array_push($geojson['features'], $c);
    }
}
header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
$conn = NULL;
?>