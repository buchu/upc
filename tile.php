<?php
$m = new MongoClient();
$db = $m->upc2;
$spotsCollection = new MongoCollection($db, 'spots');
$borderSpotsCollection = new MongoCollection($db, 'border_spots');


function pixToLat($y) {
    $globalTopLeftLat = $GLOBALS['rangelatlng']['topleft']['lat'];
    $globalTopRightLat = $GLOBALS['rangelatlng']['bottomleft']['lat'];

    $a=$globalTopLeftLat - $globalTopRightLat;
    $a /= 1024;

    return $globalTopLeftLat + $y*$a;
}

function pixToLng($x) {
    $globalTopLeftLng = $GLOBALS['rangelatlng']['topleft']['lng'];
    $globalTopRightLng = $GLOBALS['rangelatlng']['topright']['lng'];

    $a=$globalTopRightLng - $globalTopLeftLng;
    $a /= 1024;

    return $globalTopLeftLng - $x*$a;
}

function calculateLat($y, $z) {
    $pi = '3.14159265359';
    $n = (string) pow(2, $z);

    $g1 = bcmul('2', (string) $y, 15);
    $l1 = bcdiv($g1, $n, 15);
    $a1 = bcsub('1', $l1, 15);
    $var = bcmul($pi, $a1, 15);

    return rad2deg(atan(sinh($var)));
}

function calculateLon($x, $z) {
    $n = (string) pow(2, $z);
    $a1 = bcdiv((string) $x, (string) $n, 15);
    $a2 = bcmul($a1, '360.0', 15);
    $a3 = bcsub($a2, '180.0', 15);

    return $a3;
}

function calculateY($lat, $z) {
    $pi = '3.14159265359';

    $a1 = (string) cos(deg2rad($lat));
    $a2 = (string)  tan(deg2rad($lat));
    $a3 = bcdiv('1', $a1, 15);
    $a4 = (string) log($a2 + $a3);
    $a5 = bcdiv($a4, $pi, 15);
    $a6 = bcsub('1', $a5, 15);
    $a7 = bcdiv($a6, '2', 15);
    $a8 = (string) pow(2, $z);
    $a9 = bcmul($a7, $a8, 15);

    return $a9;
}

function calculateX($lon, $z) {
    $a1 = bcadd((string) $lon, '180', 15);
    $a2 = bcdiv($a1, '360', 15);
    $a3 = (string) pow(2, $z);
    $a4 = bcmul($a2, $a3, 15);

    return $a4;
}

function distance($lat1, $lon1, $lat2, $lon2) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  
  return $miles * 1.609344;
}

function draw($draw, $size, $x, $y, $count) {
    $size /= 2;
    for($i=0; $i<6; $i++) {
        $coordinates[] = array(
            'x' => $x + $size*cos($i*2*pi()/6),
            'y' => $y + $size*sin($i*2*pi()/6)
        );
    }
    $enabled = False;
    foreach($coordinates as $c){
        if($c['x']>= 256 and $c['x']<=512 and $c['y']>=256 and $c['y']<=512){
            $enabled = True;
        }
    }
    // DEV ONLY
    // $custom = False;
    // foreach($coordinates as $c){
    //     if($c['x']< 256 or $c['x']>=512 or $c['y']<=256 or $c['y']>=512){
    //         $custom = True;
    //     }
    // }
    // END DEV
    if($enabled) {
        foreach ($coordinates as $key => $value) {
        $latlancoords[] = array(
                'lat' => pixToLat($value['y']),
                'lng' => pixToLng($value['x'])
            );
        }
        $a = distance($latlancoords[1]['lat'], $latlancoords[1]['lng'], $latlancoords[2]['lat'], $latlancoords[2]['lng']);
        $s = (3 * pow($a, 2) * sqrt(3)) / 2;
        $av = bcdiv($count, $s);

        if($av < 3*$GLOBALS['average']){
            $color = "#a3bdff";
        } elseif ($av > 3*$GLOBALS['average'] and $av < 6*$GLOBALS['average']) {
            $color = "#6087e7";
        } else {
            $color = "#224499";
        }
        // DEV ONLY
        // if($custom){
        //     $draw->setFillColor("#EE0000");
        // }
        // END DEV
        foreach($coordinates as $c){
            if($c['x']<=256 or $c['x']>=512 or $c['y']<=256 or $c['y']>=512){
                $z = $GLOBALS['tile_z'];
                $lat = pixToLat($y);
                $lng = pixToLng($x);
                $radius  = $a*10;     
                $db = $GLOBALS['db'];
                $borderSpotsCollection = $GLOBALS['borderSpotsCollection'];
                $queryMongo = array("lat" => array('$gt' => ($lat-$radius),
                                   '$lt' => ($lat+$radius)),
                    "lng" => array('$gt' => ($lng-$radius),
                                   '$lt' => ($lng+$radius))
                );
                $cursor = $borderSpotsCollection->find($queryMongo)->limit(1);
                $confs = iterator_to_array($cursor);
                if(!$confs){
                    $borderSpotsCollection->insert(array("lat"=>$lat,"lng"=>$lng,"color"=>$color));
                } else {
                    $color = array_values($confs)[0]['color'];
                }
                break;
            }
        }   
        // print_r("after");exit;
        $draw->setFillColor($color);
        $draw->setFillOpacity(0.7);
        $draw->polygon($coordinates); 
    }
}


// Get vars
$tile_x = @$_GET['x'];
$tile_y = @$_GET['y'];
$tile_z = @$_GET['z'];
$size = 15;
$a = $size / 2;
$b = $a / 2;
$c = sqrt( ($a * $a) + ($b * $b) );
$width = ($b + $b + $c);
$height = ($b + $b + $c);
    
// Create image
$image = new Imagick();
$image->newImage(768, 768, new ImagickPixel( 'transparent' ));
$draw = new ImagickDraw();

// Calc tile range
$rangexy['topleft']['x'] = $tile_x - 1;
$rangexy['topleft']['y'] = $tile_y - 1;

$rangexy['topright']['x'] = $tile_x + 2;
$rangexy['topright']['y'] = $tile_y - 1;

$rangexy['bottomleft']['x'] = $tile_x - 1;
$rangexy['bottomleft']['y'] = $tile_y + 2;

$rangexy['bottomright']['x'] = $tile_x + 2;
$rangexy['bottomright']['y'] = $tile_y + 2;

foreach ($rangexy as $key => $loc) {
    $rangelatlng[$key]['lat'] = calculateLat($loc['y'], $tile_z);
    $rangelatlng[$key]['lng'] = calculateLon($loc['x'], $tile_z);
}
// print_r($rangelatlng);exit;
$xSpread = distance(52, $rangelatlng['topleft']['lng'], 52, $rangelatlng['topright']['lng']);
$ySpread = distance($rangelatlng['bottomleft']['lat'], 4, $rangelatlng['topleft']['lat'], 4);
$surface = bcmul($xSpread, $ySpread);

$queryMongo = array("lat" => array('$gt' => (string)$rangelatlng['bottomleft']['lat'],
                                   '$lt' => (string)$rangelatlng['topleft']['lat']),
                    "lng" => array('$gt' => (string)$rangelatlng['topleft']['lng'],
                                   '$lt' => (string)$rangelatlng['topright']['lng'])
        );


$cursor = $spotsCollection->find($queryMongo);

$spotsdraw = array();
foreach($cursor as $spot => $val){
    $precision = 15;

    // Calc x and y for spot
    $spot_x = (string) calculateX($val['lng'], $tile_z);
    $spot_y = (string) calculateY($val['lat'], $tile_z);
    
    // Remove tile width
    $spot_x_in_tile = (string) bcsub($spot_x, $rangexy['bottomleft']['x'], $precision);
    $spot_y_in_tile = (string) bcsub($spot_y, $rangexy['topleft']['y'], $precision);
    
    // Calculate pixels
    $spot_left_in_tile = ((string) bcmul($spot_y_in_tile, '256', 5));
    $spot_top_in_tile = ((string) bcmul($spot_x_in_tile, '256', 5));
    
    // Group to hexagons
    $w = (string) ($size);
    $h = (string) ($size) + 2;

    $hex_left_in_tile = bcmul(bcdiv($spot_left_in_tile, $h, 0), $h, 0);
    $hex_top_in_tile = bcmul(bcdiv($spot_top_in_tile, $w, 0), $w, 0);


    // Calculate pixels in total map
    $spot_top_in_map = round((string) bcmul($spot_x, '1', 5));
    // echo $spot_top_in_map;  exit;
    // $hex_top_in_map = bcmul((bcdiv($spot_top_in_map, $w, 0)), $w, 0);

    $spot_top_in_map = $hex_top_in_tile;

    // Add to draw array
    if ( ! isset($spotsdraw[$hex_top_in_tile]))
        $spotsdraw[$hex_top_in_tile] = array(
            'map_hex' => array(
                'top' => $spot_top_in_map
            )
        );

    if ( ! isset($spotsdraw[$hex_top_in_tile][$hex_left_in_tile]))
        $spotsdraw[$hex_top_in_tile][$hex_left_in_tile] = array('count' => 0);
    
    $spotsdraw[$hex_top_in_tile][$hex_left_in_tile]['count']++;
}

// Draw
ksort($spotsdraw);
$drawable = array();

// print_r($spotsdraw); exit;
foreach ($spotsdraw as $spottop => $spots) {
    if ($rangexy['bottomleft']['x'] % 2 == 0)
        $fix = ($spots['map_hex']['top'] % 2 == 0);
    else
        $fix = ! ($spots['map_hex']['top'] % 2 == 0);

    # $fix = ($spots['map_hex']['top'] % 2 == 0);

    foreach ($spots as $spotleft => $data) {
        if ($spotleft == 'map_hex') continue;

        if ($fix) {
            $spotleft = ($spotleft + ($size / 2) + 1);
        }
        $count = $data['count'];
        // make larger
        $dsize = bcmul($size, 1.30, 3);
         // $dsize = $size;

        // draw($draw, $dsize, $spottop, $spotleft);
        array_push($drawable, array("draw"=>$draw, "size"=>$dsize, "spottop"=>$spottop, "spotleft"=>$spotleft, "count"=>$count));

    }
}
$sortArray = array();

$spotsNumber = 0;
foreach ($drawable as $index => $spotData){
    $spotsNumber += $spotData['count'];
}
$average = bcdiv($spotsNumber, $surface);
if($drawable){
    foreach($drawable as $spot){ 
        foreach($spot as $key=>$value){ 
            if(!isset($sortArray[$key])){ 
                $sortArray[$key] = array(); 
            } 
            $sortArray[$key][] = $value; 
        } 
    }
    // print_r($drawable);exit;
    array_multisort($sortArray["count"],SORT_ASC,$drawable);
    $border=count($drawable)/3;
    foreach ($drawable as $index => $spotData){
        if($index <= $border){
            draw($spotData['draw'], $spotData['size'], $spotData['spottop'], $spotData['spotleft'], $spotData['count']);
        } elseif ($index > $border and $index <= 2*$border){
            draw($spotData['draw'], $spotData['size'], $spotData['spottop'], $spotData['spotleft'], $spotData['count']);
        } else {
            draw($spotData['draw'], $spotData['size'], $spotData['spottop'], $spotData['spotleft'], $spotData['count']);
        }
        
    }   
}
// Output image
// print_r($average);exit;
$image->drawImage($draw);

if ( ! isset($_GET['nocrop']) )
    $image->cropImage(256, 256, 256, 256);
$image->setImageFormat('png');
// $image->setImageCompressionQuality(100);
// $path = sprintf("images/%d-%d-%d.png", $tile_z, $tile_x, $tile_y);
// $image->writeImage($path);  

header("Content-type: image/png");
echo $image;