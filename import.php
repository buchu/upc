<?php
// mysql_connect('localhost', 'root', 'sylwia123');
// mysql_select_db('upc');
$m = new MongoClient();
$db = $m ->upc2;
$spotsCollection = new MongoCollection($db, 'spots');


/*

CREATE TABLE `spots` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lat` float(9,6) DEFAULT NULL,
  `lng` float(9,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lat` (`lat`,`lng`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 */

$path = 'data.txt';
$delimiter = "\t";
$enclosure = '"';

$handle = fopen($path, 'r');

if ($handle) {

    while ($data = fgetcsv($handle, 0, $delimiter, $enclosure))
    {
        $lat = str_replace(',', '.', $data[4]);
        $lng = str_replace(',', '.', $data[3]);

        // mysql_query('INSERT INTO spots (lat, lng) VALUES (' . $lat . ', ' . $lng . ')');
        $spotsCollection->insert(array('lat'=>$lat,'lon'=>$lng));
    }
} else {
    throw new Exception('Could not open file');
}