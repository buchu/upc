$(function() {
    var key     = '80dc1786b05442d89e05e8abec8dc527', // f: 80dc1786b05442d89e05e8abec8dc527
        center  = [52.38743, 5.23165],
        zoom    = 8,
        maxZoom = 15,
        minZoom = 7,
        typingTimer;

    // Create map
    var map = L.map('map', {
        /*maxBounds: [
            [53.620542, 3.212891],
            [50.750925, 6.026306],
        ],*/
        attributionControl: false
    }).setView(center, zoom);

    // add map layer
    L.tileLayer('http://{s}.tile.cloudmade.com/' + key + '/123789/256/{z}/{x}/{y}.png', {
        maxZoom: maxZoom,
        minZoom: minZoom
    }).addTo(map);

    // add map layer of wifispots (images)
    // L.tileLayer('images/{z}-{x}-{y}.png', {
    //     maxZoom: maxZoom,
    //     minZoom: minZoom
    // }).addTo(map);

    // add map layer of wifispots (live generator)
    L.tileLayer('tile.php?z={z}&x={x}&y={y}', {
        maxZoom: maxZoom,
        minZoom: minZoom
    }).addTo(map);

    // hide results div
    $('#results').hide().slideUp().empty();

    // Autocomplete results
    var autoComplete = function() {
        var $input  = $('#mapContainer #search input.keyword'),
            keyword = $input.val();

        // Search when we have input
        if (keyword.length > 0) {
            $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?sensor=true&region=NL&language=nl&address=' + keyword + ', Nederland', function(data) {
                // Check if we have a result
                if (data.status == 'OK' && data.results.length > 0) {
                    var items = [];

                    $.each(data.results, function(key, val) {
                        if (key >= 5) return false; // limit 5

                        items.push(
                            '<li><a href="#" data-lat="' + val.geometry.location.lat + '" data-lng="' + val.geometry.location.lng + '">' + val.formatted_address + '</a></li>'
                        );
                    });

                    // Set search results
                    $('#mapContainer #results').empty();

                    $('<ul/>', {
                        html: items.join('')
                    }).appendTo('#results');

                    $('#mapContainer #results').slideDown(100);
                }
            });
        } else {
            // Clear search results
            $('#results').empty();
        }
    }

    // Events
    $('#mapContainer #search .keyword').keyup(function (e) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(autoComplete, 800);
    });

    $('#mapContainer #search .keyword').keydown(function (e) {
        clearTimeout(typingTimer);
    });

    $('#mapContainer #search').submit(function(e) {
        e.preventDefault();

        var $input  = $('#mapContainer #search input.keyword'),
            keyword = $input.val();

        $input.val('Laden...').prop('readonly', 'readonly');

        $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?sensor=true&region=NL&language=nl&address=' + keyword + ', Nederland', function(data) {
            // Check if we have a result
            if (data.status == 'OK' && data.results.length > 0) {
                // Focus map
                var loc = new L.LatLng(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
                map.setView(loc, 14);

                // Clear search box
                $input.val('').prop('readonly', false);

                // Clear results
                clearTimeout(typingTimer);

                $('#mapContainer #results').hide().slideDown().empty();
            } else {
                // Show error message
                $input.val('-- Geen resultaten gevonden --').prop('readonly', 'readonly');

                // Remove message
                setTimeout(function (){
                    $input.val('').prop('readonly', false);
                }, 2000);
            }
        });
    });

    $(document).on('click', '#mapContainer #results a', function(e) {
        e.preventDefault();

        // Focus map
        var loc = new L.LatLng($(this).data('lat'), $(this).data('lng'));
        map.setView(loc, 14);

        // Remove results
        $('#mapContainer #results').hide().slideUp().empty();

        // Empty search input
        $('#mapContainer #search input.keyword').val('');
    });
});
